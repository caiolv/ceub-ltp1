numero = int(input("Informe o número: "))

def is_prime(x):
    if x == 0:
        return False
    elif x == 1:
        return False
    elif x == 2:
        return True
    for n in range(3, x-1):
        if x % n == 0:
            return False
    return True

if(is_prime(numero)):
    print("O número é primo.")
else:
    print("O número não é primo.")
