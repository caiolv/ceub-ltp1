def imprimir_tipos(lista_tipos):
    for tipo in lista_tipos:
        print(tipo)

buffet = ("massas", "carnes", "acompanhamentos", "saladas", "sobremesas")
imprimir_tipos(buffet)

# buffet[0] = "risotos" #TypeError

buffet = ("risotos", "opção de proteina", "acompanhamentos", "saladas", "sobremesas")
imprimir_tipos(buffet)
