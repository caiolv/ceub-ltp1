#A  'Nota de Aula 3' possui os mesmo exerícios que a 'Nota de Aula 2'
cebolas = 100 #Define uma variavel e atribui um valor a ela
cebolas_na_caixa = 150 #Define uma variavel e atribui um valor a ela
espaco_caixa = 4 #Define uma variavel e atribui um valor a ela
caixas = 30 #Define uma variavel e atribui um valor a ela
cebolas_fora_da_caixa = cebolas - cebolas_na_caixa #Define uma variavel e atribui um valor de uma expressão a ela
caixas_vazias = caixas - (cebolas_na_caixa/espaco_caixa) #Define uma variavel e atribui um valor de uma expressão a ela
caixas_necessarias = cebolas_fora_da_caixa / espaco_caixa #Define uma variavel e atribui um valor de uma expressão a ela
print ("Existem", str(cebolas_na_caixa), "cebolas encaixotadas") #Imprime na tela uma string concatenada com o valor de cebolas na caixa e outra string
print ("Existem", str(cebolas_fora_da_caixa), "cebolas sem caixa") #Imprime na tela uma string concatenada com o valor de cebolas fora da caixa e outra string
print ("Em cada caixa cabem", str(espaco_caixa), "cebolas") #Imprime na tela uma string concatenada com o valor de espaço na caixa e outra string
print ("Ainda temos,", str(caixas_vazias), "caixas vazias") #Imprime na tela uma string concatenada com o valor de caixas vazias e outra string
print ("Então, precisamos de", str(caixas_necessarias), "caixas para empacotar todas as cebolas") #Imprime na tela uma string concatenada com o valor de caixas necessarias e outra string
