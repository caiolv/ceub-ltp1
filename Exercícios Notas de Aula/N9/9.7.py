lista = ['roma', 'ramo', 'flor']

def confirmar_anagrama(palavra_escolhida, palavra):
    for letra in palavra:
        if(letra in palavra_escolhida):
            continue
        else:
            return False
    return True

def identificar_anagramas(palavra_escolhida = ""):
    anagramas = []
    for palavra in lista:
        if(len(palavra) == len(palavra_escolhida)):
            if (confirmar_anagrama(palavra_escolhida, palavra)):
                anagramas.append(palavra)

    print("Na lista fornecida, essas sao as palavras anagramas de " + palavra_escolhida)
    print(anagramas)

identificar_anagramas('amor')
