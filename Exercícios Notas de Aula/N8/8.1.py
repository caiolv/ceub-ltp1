lugares = ["Montreal", "Moscou", "Sidney", "Cidade do México", "Londres"]
print(lugares)

print(sorted(lugares))
print(sorted(lugares, reverse=True))
print(lugares)

lugares.reverse()
print(lugares)

lugares.reverse()
print(lugares)

lugares.sort()
print(lugares)

lugares.sort(reverse=True)
print(lugares)
