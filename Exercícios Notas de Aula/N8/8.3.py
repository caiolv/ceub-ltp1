linguas = ["Português", "Francês", "Alemão", "Italiano", "Japonês", "Coreano"]
print(linguas)
print(sorted(linguas))
print(sorted(linguas, reverse=True))

linguas.reverse()
print(linguas)
linguas.sort()
print(linguas)
linguas.sort(reverse=True)
print(linguas)
print(len(linguas))
