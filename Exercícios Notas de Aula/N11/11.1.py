numeros = [valor for valor in range(1, 1000001)]

primeiro_do_meio = int(len(numeros)/2 - 1)

print("Os primeiros 3 itens da lista são:", numeros[0:3])
print("Os últimos 3 itens da lista são: ", numeros[-3:])
print("Os 3 itens no meio da lista são: ", numeros[primeiro_do_meio:primeiro_do_meio+3])
