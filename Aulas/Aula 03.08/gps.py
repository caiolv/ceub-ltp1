cost = 0
distance = ""
avarage_price_gasoline = 4.40
invalidDistance = True
avarage_km_liter = 12
perfil = ""
time = 0
avarage_speed = 60

while invalidDistance:

    try:
        distance = float(input('Digite a distancia (em kilometros): '))
        invalidDistance = False

    except ValueError:
        print('A distancia deve ser um número: ')

while perfil not in ["agressivo", "seguro", "economico"]:
    perfil = input('Informe seu perfil (agressivo, seguro ou economico): ')

if perfil in "agressivo": avarage_speed = 120
if perfil in "seguro": avarage_speed = 100
if perfil in "economico": avarage_speed = 80

total_hours = distance/avarage_speed
cost = (distance/avarage_km_liter)*avarage_price_gasoline

print("A viagem informada durará " + str(total_hours) + " horas e custará " + str(cost) + " reais.")
