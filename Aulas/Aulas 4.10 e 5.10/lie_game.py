
'''
Caio Costa de Oliveira - 21852202
Jorge Augusto Carvalho - 21700429
Mattheus Patricio Matos - 21750786
'''
import os
import random

cartas = {
    "paus" : ["A", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K"],
    "ouros" : ["A", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K"],
    "copas" : ["A", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K"],
    "espadas" : ["A", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K"]
}

#obter carta aleatoria
def get_card():
    card = []
    num = random.randint(0, 12)
    naipe = random.choice(list(cartas.items()))[0]
    card.append(naipe)
    card.append(cartas[naipe][num])

    return card
def get_card_except(drawn_card):
    card = get_card()
    if(card == drawn_card):
        return get_card_except(drawn_card)
    else:
        return card
#decidir verdade ou mentira
def should_lie():
    return random.choice([True, False])
def show_card(card):
    print("A carta tirada foi", card[1], "de", card[0] + ".")
def get_user_answer():
    print("Selecione uma opcão:")
    print("1 - Verdade")
    print("2 - Mentira")
    return get_user_input_answer()

def get_user_input_answer():
    opcoes = {"1":True, "2":False}
    try:
        opcao = input("Opcão:")
        return opcoes[opcao]
    except KeyError:
        print("Opção inválida.")
        return get_user_input_answer()
#verificar resposta
def verify_answer(choice, drawn_card, tf_card):
    if(choice):
        return (drawn_card == tf_card)
    else:
        return (drawn_card != tf_card)

def show_score(computer_points, player_points):
    os.system("clear")
    print("--- PONTUACAO ---")
    print("Computador:", str(computer_points))
    print("Player:", str(player_points))
    print("-----------------\n")
def end_game(computer_points, player_points):
    show_score(computer_points, player_points)
    if(computer_points > player_points):
        print("O computador ganhou!")
    else:
        print("Você ganhou!")
#loop principal
def loop_game():
    player_points = 0
    computer_points = 0

    while(player_points < 3 and computer_points < 3):
        show_score(computer_points, player_points)
        drawn_card = get_card()
        tf_card = drawn_card

        if (should_lie()):
            tf_card = get_card_except(drawn_card)

        show_card(tf_card)

        user_answer = get_user_answer()
        correct = verify_answer(user_answer, drawn_card, tf_card)

        if(correct):
            player_points += 1
        else:
            computer_points += 1

    end_game(computer_points, player_points)

    return
def exit():
    print("Até mais.")
    return

functions = {"1" : loop_game, "0": exit}

def show_menu():
	print("Seja Bem Vindo ao Jogo da Mentira")
	print("---------------------------")
	print("1 - Iniciar Jogo")
	print("0 - Sair\n")

def get_option_menu():
    try:
        option = input("Opção: ")
        functions[option]()
        if (option != "0"):
            input("\nAperte enter para voltar ao menu.")
    except KeyError:
        print("Opção inválida.")
        return get_option_menu()

    return option
def menu():
	os.system("clear")
	show_menu()
	return get_option_menu()

def start():
    option = "enter"
    while option != "0":
        option = menu()
    return

start()
