from random import *

'''
Definição do tabuleiro da roleta.
O tabuleiro é representado por um dicionário CHAVE-VALOR.

CHAVE: valor númerico inteiro da casa do tabuleiro.
VALOR: lista com as características associadas a casa do tabuleiro.

    NUMERO:  [<COR>,<TERCO>,<METADE>,<PARIDADE>], onde:

	NUMERO   | string | valores no intervalo de 0 a 36
	COR      | string | "black", "red" ou "green"
	TERCO    | string | "first", "second", "third" ou "none"
	METADE   | string | "first-half", "second-half" ou "none"
	PARIDADE | string | "odd", "even" ou "none"

'''
roulette_board = {0: ["green" , "none"  , "none"       , "none"],
			      1: ["black" , "first" , "first-half" , "odd" ],
				  2: ["red"   , "first" , "first-half" , "even"],
				  3: ["black" , "first" , "first-half" , "odd" ],
				  4: ["red"   , "first" , "first-half" , "even"],
				  5: ["black" , "first" , "first-half" , "odd" ],
				  6: ["red"   , "first" , "first-half" , "even"],
				  7: ["black" , "first" , "first-half" , "odd" ],
				  8: ["red"   , "first" , "first-half" , "even"],
				  9: ["black" , "first" , "first-half" , "odd" ],
				  10:["red"   , "first" , "first-half" , "even"],
				  11:["black" , "first" , "first-half" , "odd" ],
				  12:["red"   , "first" , "first-half" , "even"],
				  13:["black" , "second", "first-half" , "odd" ],
				  14:["red"   , "second", "first-half" , "even"],
				  15:["black" , "second", "first-half" , "odd" ],
				  16:["red"   , "second", "first-half" , "even"],
				  17:["black" , "second", "first-half" , "odd" ],
				  18:["red"   , "second", "first-half" , "even"],
				  19:["black" , "second", "second-half", "odd" ],
				  20:["red"   , "second", "second-half", "even"],
				  21:["black" , "second", "second-half", "odd" ],
				  22:["red"   , "second", "second-half", "even"],
				  23:["black" , "second", "second-half", "odd" ],
				  24:["red"   , "second", "second-half", "even"],
				  25:["black" , "third" , "second-half", "odd" ],
				  26:["red"   , "third" , "second-half", "even"],
				  27:["black" , "third" , "second-half", "odd" ],
				  28:["red"   , "third" , "second-half", "even"],
				  29:["black" , "third" , "second-half", "odd" ],
				  30:["red"   , "third" , "second-half", "even"],
				  31:["black" , "third" , "second-half", "odd" ],
				  32:["red"   , "third" , "second-half", "even"],
				  33:["black" , "third" , "second-half", "odd" ],
				  34:["red"   , "third" , "second-half", "even"],
				  35:["black" , "third" , "second-half", "odd" ],
				  36:["red"   , "third" , "second-half", "even"]}

bets = []
low_risk = ["red", "black", "odd", "even", "first-half", "second-half"]
medium_risk = ["first", "second", "third"]
number_drawn = 0

'''
Funções para verificação das cores de um determinado valor
'''

def check_color(value, color):
	try:
		return roulette_board[value][0] == color
	except KeyError:
		# TODO: deve ser criado um erro específico para esse caso
		return "ERRO DE TABULEIRO"

def is_red(value):
	return check_color(value, "red")

def is_black(value):
	return check_color(value, "black")

def is_green(value):
	return check_color(value, "green")



'''
Funções básicas
'''
# função que fornecido um valor e um terço indica se o valor é pertencente a esse terço
def check_third_part(value, third_part):
	try:
		return roulette_board[value][1] == third_part
	except KeyError:
		# TODO: deve ser criado um erro específico para esse caso
		return "ERRO DE TABULEIRO"

# função que indica se uma determinada casa está no primeiro terço
def is_first_third_part(value):
    return check_third_part(value, "first")
# função que indica se uma determinada casa está no segundo terço
def is_second_third_part(value):
    return check_third_part(value, "second")
# função que indica se uma determinada casa está no terceiro terço
def is_third_third_part(value):
    return check_third_part(value, "third")

# função que fornecido um valor e uma metade indica se o valor é pertencente a essa metade
def check_half(value, half):
	try:
		return roulette_board[value][2] == half
	except KeyError:
		# TODO: deve ser criado um erro específico para esse caso
		return "ERRO DE TABULEIRO"
# função que indica se uma determinada casa está na primeira metade
def is_first_half(value):
    return check_half(value, "first")
# função que indica se uma determinada casa está na segunda metade
def is_second_half(value):
    return check_half(value, "second")

def check_parity(value, parity):
	try:
		return roulette_board[value][3] == parity
	except KeyError:
		# TODO: deve ser criado um erro específico para esse caso
		return "ERRO DE TABULEIRO"

# função que indica se o número é par
def is_even(value):
	return check_parity(value, "even")
# função que indica se o número é impar
def is_odd(value):
	return check_parity(value, "odd")

'''
Funções práticas
'''

verify_bet_functions = {"red"        : is_red,
                 "black"      : is_black,
                 "first"      : is_first_third_part,
                 "second"     : is_second_third_part,
                 "third"      : is_third_third_part,
                 "first-half" : is_first_half,
                 "second-half": is_second_half,
                 "even"       : is_even,
                 "odd"        : is_odd}
# função que fornecida uma aposta e a casa sorteada indica se a aposta foi vencedora
def check_bet(bet, house):
    return (str(house) == bet) or (bet in roulette_board[house])

# função que gera um valor aleatório entre 0 e 36
def spin_roulette():
    return randint(0, 36)

'''
Funções avançadas
'''

# função que fornecida a aposta e o valor da aposta, retorne o lucro ou prejuizo
# uma aposta em um número dobra o valor apostado
# uma aposta em um terco aumenta em 20% o valor apostado
# uma aposta em uma metade, cor, ou paridade, aumenta em 5% o valor apostado
def make_bet(bet_value, bet):
    if(check_bet(bet)):
        if(bet in low_risk):
            return bet_value * 1.05
        elif(bet in medium_risk):
            return bet_value * 1.2
        else:
            return bet_value * 2
    else:
        return -bet_value

# função que armazene as apostas realizadas e o nome do apostador

# função que verifica em uma lista de apostas o resultado de cada uma delas

'''
Funções especiais
'''

# função que fornecido o nome de uma pessoa retorne quanto ela tem de lucro ou de prejuizo

# crie uma validação que não permita que uma pessoa aposte caso ela esteja devendo mais que 1000 reais

# função que execute um menu da seguinte forma:
	# 1. apostar
	# 2. fechar ciclo de apostas
	# 3. fechar a banca

    # a opção 1 adiciona uma nova aposta na lista de apostas.
    # a opção 2 realiza o sorteio e indica os vencedores com seus respectivos lucros ou prejuizos
    # a opção 3 fecha o programa, informando o saldo da banca.
'''
Pré-requisitos
'''

# Ao iniciar o jogo a banca sempre começa com o saldo de R$ 100.000,00

# Caso algum apostador ganhe deve ser retirado o dinheiro da banca.
# Caso contrário deve ser adicionado o valor da aposta na banca.

# O jogo deve funcionar completamente.
# Todos os erros possíveis devem ser tratados.
