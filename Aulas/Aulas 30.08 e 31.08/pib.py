import os
from collections import OrderedDict

countries = {}

def ask_country_name():
    country = str(input("Digite o nome do pais: "))
    if(country.upper() in countries):
        print("Esse pais ja foi cadastrado.")
        return ask_country_name()

    return country

def ask_country_pib():
    try:
    	pib = float(input("Digite o PIB do pais: "))
    except ValueError:
    	print("Insira um numero decimal.")
    	return ask_country_pib()

    return pib

def add_country():
    country = ask_country_name()
    pib = ask_country_pib()

    countries[country.upper()] = pib

    return

def list_ranking(msg_search):
    sum = 0

    os.system("clear")

    print("Ranking: \n")
    order_pib()

    for key, value in countries.items():
        sum += value
    print("------------------------")
    print("Somatório PIB:", sum)

    search_ranking(msg_search)

    return

def order_pib():
    sorted_countries = OrderedDict(reversed(sorted(countries.items(), key=lambda x: x[1])))
    if(sorted_countries):
        for key, value in sorted_countries.items():
            print(key.title(), value)
    else:
        print("Nenhum pais foi inserido.")

def search_ranking(msg_search):
    search_country = str(input(msg_search))
    if(search_country.upper() in countries):
        print(search_country.title(), countries[search_country.upper()])
        return search_ranking(msg_search)
    elif(search_country == ""):
        return
    else:
        print("Pais nao encontrado.")
        return search_ranking(msg_search)

def run():
    running = True
    while(running):
        os.system("clear")
        print("------------------------")
        print("Selecione uma opcao:")
        print("1 - Inserir um pais")
        print("2 - Listar Ranking")
        print("3 - Sair")

        opcao_invalida = True

        while(opcao_invalida):
            opcao = input()
            if(opcao == "1"):
                add_country()
                opcao_invalida = False
            elif(opcao == "2"):
                list_ranking("Digite o nome de um pais da lista ou enter para voltar ao menu:\n")
                opcao_invalida = False
            elif(opcao == "3"):
                list_ranking("Digite o nome de um pais da lista ou enter para finalizar o programa:\n")
                running = False
                opcao_invalida = False
            else:
                print("Insira um numero dentre as opções do menu.")
                opcao_invalida = True

run()
