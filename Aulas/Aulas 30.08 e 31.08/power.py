import os
running = True

def power(valor1, valor2):
    return valor1**valor2

def pedirValor(index):
    valorInalido = True
    while(valorInalido):
        try:
            valor = int(input("Insira o " + str(index) + "o valor: "))
            valorInalido = False
        except ValueError:
            print("O valor necessita ser inteiro.")
            valorInalido = True
    return valor

def realizarCalculo():
    valor1 = pedirValor("1")
    valor2 = pedirValor("2")

    print("Resultado:", power(valor1, valor2))
    input("Aperte enter para voltar ao menu")

while(running):
    os.system("clear")
    print("------------------------")
    print("Selecione uma opção:")
    print("1 - Realizar cálculo")
    print("2 - Sair do programa")

    opcao_invalida = True

    while(opcao_invalida):
        opcao = input()
        if(opcao == "1"):
            realizarCalculo()
            opcao_invalida = False
        elif(opcao == "2"):
            running = False
            opcao_invalida = False
        else:
            print("Insira um numero dentre as opções do menu.")
            opcao_invalida = True
