'''
Projeto de caixa eletrônico que realiza
saque, extrato, transferência e deposito
'''

from atm_model import *
from atm_interface import *

accounts = {"AX476T": ["José Renato Martins", "007.008.009-00", 23.56],
			"BF896T": ["Fulano", "007.007.007-77", 2847233.56],
			"BATMAN": ["Batman", "000.000.000-01", 289999947233.56],
			"AXXX67": ["Iron Man", "123.007.007-77", -522324341.56]}

def ask_value(action):
	value = 0
	try:
		value = float(input("Informe o valor que deseja " + action + ": "))
	except ValueError:
		print("O valor precisa ser um número.")
		ask_value(action)

	return value

def withdraw(identity):
	value = ask_value("sacar")
	accounts[identity][2] -= value
	return

def deposit(identity):
	value = ask_value("depositar")
	accounts[identity][2] += value
	return

def extract(identity):
	print("O saldo da conta é", "{0:.2f}".format(accounts[identity][2]))
	input("Aperte enter para voltar ao menu.")

def transfer(identity):
	print("transfer!")

def close(identity):
	print("Bye Bye!")
	exit()


functions = {"1" : withdraw, "2": deposit, "3": extract, "4": transfer, "0": close}


def askID():
	ID = input("Type your ID: ")
	return ID

def showOptions():
	option = input("---------------------------\n" +
		  "1 - Sacar\n" +
		  "2 - Depositar\n" +
		  "3 - Extrato\n" +
		  "4 - Transferência\n" +
		  "0 - Sair\n\n" +
		  "Opção: ")

	return option


def checkID(identity):

	try:
		accounts[identity]
	except KeyError:
		return False

	return True
def ask_option():
	option = True
	while option != "0":
		try:
			option = showOptions()
			functions[option](identity)
		except KeyError:
			print("Opção inválida.")
			ask_option()

def start():
	identity = askID()
	valid    = checkID(identity)

	if not valid:
		print("ERRO. USUÁRIIO NÃO IDENTIFICADO")
		return

	ask_option()

start()
