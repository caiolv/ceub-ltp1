accounts = {"AX476T": ["José Renato Martins", "007.008.009-00", 23.56],
			"BF896T": ["Bolsonaro", "007.007.007-77", 2847233.56],
			"BATMAN": ["Batman", "000.000.000-01", 1000.56],
			"AXXX67": ["Iron Man", "123.007.007-77", -522324341.56]}

'''
Verifica se o ID informado existe no banco de dados
Retorna True se existir e False caso contrario
'''
def check_id(identity):

	try:
		accounts[identity]
	except KeyError:
		return False

	return True
'''
Realizar saque da conta informada
Retorna True se o saque foi realizado e False caso contrario
'''
def perform_withdraw(identity, value):

	if not check_id(identity):
		return False

	accounts[identity][2] -= value
	return True

'''
Realizar deposito da conta informada
Retorna True se o deposito foi realizado e False caso contrario
'''
def perform_deposit(identity, value):

	if not check_id(identity):
		return False

	accounts[identity][2] += value
	return True
