'''
Pede uma string e a retorna
'''
def ask_string(message):
	v = input(message)
	return v
'''
Pede um valor inteiro
Realiza o pedido ate ser informado um valor valido
Retorna o valor inserido
'''
def ask_int(message, error_message = "not a int value"):
	try:
		v = int(input(message))
		return v
	except ValueError:
		print(error_message)
		return ask_int(message, error_message)

'''
Pede um valor decimal
Realiza o pedido ate ser informado um valor valido
Retorna o valor inserido
'''
def ask_float(message, error_message = "not a float value"):
	try:
		v = float(input(message))
		return v
	except ValueError:
		print(error_message)
		return ask_float(message, error_message)

'''
Informa as opções do menu
'''
def show_menu():
	print("---------------------------")
	print("1 - Sacar")
	print("2 - Depositar")
	print("3 - Extrato")
	print("4 - Transferência")
	print("0 - Sair\n")
