'''
Projeto de caixa eletrônico que realiza
saque, extrato, transferência e deposito
'''
import os
from atm_interface import *
from atm_model import *
option = "enter"
'''
Manipula o Saque
Faz o pedido do valor, demanda a ação de saque e
informa ao usuário se a operação foi ou não concluída
'''
def withdraw(identity):
	os.system("clear")
	value = ask_int("Valor do saque: ")

	if not perform_withdraw(identity, value):
		print("Saque não pode ser realizado!")
		return

	print("Saque de R$" + str(value) + " realizado com sucesso.")

'''
Manipula o Depósito
Faz o pedido do valor
Demanda a ação de depósito
Informa ao usuário se a operação foi ou não concluída
'''
def deposit(identity):
	os.system("clear")
	value = ask_float("Valor do deposito: ")

	if not perform_deposit(identity, value):
		print("Deposito não pode ser realizado!")
		return

	print("Deposito de R$" + str(value) + " realizado com sucesso.")

'''
Informa ao usuário o saldo de sua conta
'''
def extract(identity):
	os.system("clear")
	print("Seu saldo atual é: " + str(accounts[identity][2]))

'''
Manipula a Transferência
Faz o pedido do beneficiado
Verifica a existência desse
Faz o pedido do valor
Demanda a ação de saque
Informa o usuário se a operação foi ou não concluída
'''
def transfer(identity):
	os.system("clear")
	benefited = ask_string("Informe o ID do beneficiado: ")
	valid    = check_id(benefited)

	if not valid:
		print("Usuário beneficiado não encontrado.")
		return

	value = ask_float("Valor da transferencia: ")

	if not perform_withdraw(identity, value):
		print("Transferencia não pode ser realizado!")
		return
	elif not perform_deposit(benefited, value):
		print("Transferencia não pode ser realizado!")
		perform_deposit(identity, value) #voltar o valor caso de erro na transferencia
		return

	print("Transferência realizada com sucesso.\n!")
'''
Apresenta uma mensagem de finalização
Encerra o programa
'''
def close(identity):
	print("Bye bye, " + accounts[identity][0].split(' ', 1)[0] + "!")
	exit()

functions = {"1" : withdraw, "2": deposit, "3": extract, "4": transfer, "0": close}
'''
Inicia o programa
Pede um ID
Verifica a existência desse
Entra no loop do menu
'''
def start():
	identity = ask_string("ID: ")
	valid    = check_id(identity)

	if not valid:
		print("ERRO. USUÁRIO NÃO IDENTIFICADO")
		return

	while option != "0":
		menu(identity)

	return
'''
Informa o menu
E prossegue para a escolha de opção
Retorna
'''
def menu(identity):
	os.system("clear")
	show_menu()
	get_option_menu(identity)
	return
'''
Pede uma das opção do menu
Direciona para a tela da função desejada
'''
def get_option_menu(identity):
	try:
		option = ask_string("Opção: ")
		functions[option](identity)
		input("\nAperte enter para voltar ao menu.")
	except KeyError:
		print("Opção inválida.")
		return get_option_menu(identity)

start()
