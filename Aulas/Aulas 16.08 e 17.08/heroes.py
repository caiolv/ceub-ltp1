#teste 1
h1 = {"wolverine", "spider-man", "batman", "hulk", "mulher-maravilha"}

#teste 2
h2 = {"batman antigo", "homem-formiga", "hulk fraco", "lanterna verde", "deadpool", "superman"}

#teste 3
h3 = {"ironman", "capitão america", "batman e robin", "pantera negra", "aquaman", "hulk azul", "space ghost", "motoqueiro fantasma"}

#teste 4 
h4 = {"ironman", "capitão america", "batman e robin", "pantera negra", "aquaman", "hulk azul", "space ghost", "motoqueiro fantasma", "batman antigo", "homem-formiga", "hulk fraco", "lanterna verde", "deadpool", "superman", "wolverine", "spider-man", "batman", "hulk", "mulher-maravilha"}

def remove_bad_heroes(list_heroes):
	for hero in list(list_heroes):
		if "batman" not in hero and "hulk" not in hero:
			list_heroes.remove(hero)
	return list_heroes

print("Primeira lista: ")
print(h1)
h1 = remove_bad_heroes(h1)
print(h1)

print("\nSegunda lista: ")
print(h2)
h1 = remove_bad_heroes(h2)
print(h2)

print("\nTerceira lista: ")
print(h3)
h1 = remove_bad_heroes(h3)
print(h3)

print("\nQuarta lista: ")
print(h4)
h1 = remove_bad_heroes(h4)
print(h4)
