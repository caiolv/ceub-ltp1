import os
import operator

president_candidates = {19: "Alvaro Dias", 24: "Ciro Gomes",
                        45: "Geraldo Alckmin", 21: "Jair Bolsonaro",
                        42: "Marina Silva", 13: "Lula",
                        29: "PSTU", 0: "Nulo"}

president_votes = {19: 0, 24: 0,
                   45: 0, 21: 0,
                   42: 0, 13: 0,
                   29: 0, 0: 0}

governor_candidates = {191: "Rogério Rosso", 242: "Fátima Sousa",
                       453: "General Paulo Chagas", 674: "Guillem",
                       545: "Ibaneis Rocha", 0: "Nulo"}

governor_votes = {191: 0, 242: 0,
                  453: 0, 674: 0,
                  545: 0, 0: 0}

senator_candidates = {191: "Hélio Queiroz", 242: "Izalci",
                      453: "João Pedro Ferraz", 214: "Juiz Everardo Ribeiro",
                      425: "Leila do Vôlei", 136: "Marcelo",
                      297: "Marivaldo Pereira", 982: "Paulo Roque",
                      0: "Nulo"}

senator_votes = {191: 0, 242: 0,
                 453: 0, 214: 0,
                 425: 0, 136: 0,
                 297: 0, 982: 0,
                 0: 0}

election = True

while election:

    choice = int(input("---------------------------------\n" +
                       "1. Para votar\n" +
                       "2. Para sair\n" +
                       "---------------------------------\n" +
                       "Opção: "))
    if choice == 1:
        president = input("Informe o número do seu candidato para presidente: ")

        try:
            president = int(president)
            print("O candidato escolhido foi: ", president_candidates[president])
            president_votes[president] += 1

        except (ValueError, KeyError) as a:
            confirmation = input("Você irá anular seu voto. Confirma? (S/N): ")
            if confirmation.upper() == "S":
                print("Voto anulado.")
                president_votes[0] += 1

        governor = input("Informe o número do seu candidato para governador: ")

        try:
            governor = int(governor)
            print("O candidato escolhido foi: ", governor_candidates[governor])
            governor_votes[governor] += 1

        except (ValueError, KeyError) as a:
            confirmation = input("Você irá anular seu voto. Confirma? (S/N): ")
            if confirmation.upper() == "S":
                print("Voto anulado.")
                governor_votes[0] += 1

        senator = input("Informe o número do seu candidato para senador: ")

        try:
            senator = int(senator)
            print("O candidato escolhido foi: ", senator_candidates[senator])
            senator_votes[senator] += 1

        except (ValueError, KeyError) as a:
            confirmation = input("Você irá anular seu voto. Confirma? (S/N): ")
            if confirmation.upper() == "S":
                print("Voto anulado.")
                senator_votes[0] += 1

    elif choice == 2:
        election = False

    else:
        print("Masss que burrrooo! Escolha um Número.")


winnerPresidentKey = max(president_votes.items(), key=operator.itemgetter(1))[0]
winnerGovernorKey = max(governor_votes.items(), key=operator.itemgetter(1))[0]
winnerSenatorKey = max(senator_votes.items(), key=operator.itemgetter(1))[0]

os.system("clear")
print("Resultado da eleição:")

print("Presidencia: ")
for key in president_candidates:
    print("   " + president_candidates[key] + " : " + str(president_votes[key]))
print("Presidente eleito: ", president_candidates[winnerPresidentKey])

print("\nGovernador: ")
for key in governor_candidates:
    print("   " + governor_candidates[key] + " : " + str(governor_votes[key]))
print("Governador eleito: ", governor_candidates[winnerGovernorKey])

print("\nSenador: ")
for key in senator_candidates:
    print("   " + senator_candidates[key] + " : " + str(senator_votes[key]))
print("Senador eleito: ", senator_candidates[winnerSenatorKey])
