#FAMILIA [RIVALIDADE, ATIVIDADE, CONHECIMENTOS]
familias = {
    "mortana":[45, "animais", ["succao", "carnivoro"]],
    "mariva":[87, "graos", ["succao", "plantas"]],
    "moreira":[95, "cereais", ["succao", "plantas"]]
}
#NOME [ALIMENTACAO, RARIDADE]
insetos = {
    "chenpolin":       [["carnivoro"]           , "comum"],
    "retrombis":       [["succao"]              , "raro"],
    "chumelho":        [["plantas", "succao"]   , "meio-raro"],
    "vivider":         [["plantas"]             , "comum"],
    "alteris­-funchis": [["plantas"]             , "muito-comum"],
    "Otemti":          [["carnivoro"]           , "raro"],
    "milherta":        [["carnivoro", "plantas"], "meio-raro"]
}

familia_invalida = True

while familia_invalida:
    try:
        familia_informada = str(input("Informe o nome da familia: "))
        if(familia_informada.upper() not in ["MORTANA", "MARIVA", "MOREIRA"]):
            print("Familia invalida. (mortana, mariva ou moreira)")
        else:
            familia_invalida = False

    except ValueErro:
            print("Informe por extenso o nome da familia.")


print("Insetos que a familia", familia_informada.title(), "conhece:")
for chaveFamilia, valorFamilia in familias.items():
    if(chaveFamilia.upper() == familia_informada.upper()):
        for chaveInseto, valorInseto in insetos.items():
            if(valorFamilia[2][0] in valorInseto[0] or valorFamilia[2][1] in valorInseto[0]):
                print(chaveInseto.title())
