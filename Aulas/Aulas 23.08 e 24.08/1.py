#NOME [ALIMENTACAO, RARIDADE]
insetos = {
    "chenpolin":       [["carnivoro"]           , "comum"],
    "retrombis":       [["succao"]              , "raro"],
    "chumelho":        [["plantas", "succao"]   , "meio-raro"],
    "vivider":         [["plantas"]             , "comum"],
    "alteris­-funchis": [["plantas"]             , "muito-comum"],
    "Otemti":          [["carnivoro"]           , "raro"],
    "milherta":        [["carnivoro", "plantas"], "meio-raro"]
}

tipo_invalido = True
while tipo_invalido:
    try:
        tipo_informado = str(input("Informe o tipo de alimentacao: "))
        if(tipo_informado not in ["carnivoro", "succao", "plantas"]):
            print("Tipo de alimentacao invalido. (carnivoro, succao, plantas)")
        else:
            tipo_invalido = False

    except ValueErro:
            print("Informe por extenso o tipo de alimentacao.")



for chave, valor in insetos.items():
    if(tipo_informado in valor[0]):
        print(chave.title(), "->", valor[0])
