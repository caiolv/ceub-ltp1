'''
Programa para cadastro de usuários
para clinicas nutricionais

informações solicitadas:

1. Nome completo
2. Email
3. Peso
4. Altura
5. Motivo da consulta

'''
invalidName = True

#validar tamanho min de 5
while(invalidName):
    name   = input("Nome: ")
    
    if(len(name) < 5):
        print("O nome deve ter no mínimo 5 caracteres.")
        invalidName = True
    else:
        invalidName = False

#obrigatório ter @
email  = input("Email: ")

#validar o peso como float
invalidWeight = True

while invalidWeight:
    
    try:
        weight  = float(input("Peso: "))
        invalidWeight = False

    except ValueError:
        print("O peso deve ser um número")

#validar o peso como float
invalidHeight = True

while invalidHeight:
    
    try:
        height = float(input("Altura: "))
        invalidHeight = False
    
    except ValueError:
        print("A altura deve ser um número")

#livre
reason = input("Motivo da consulta: ")
imc = weight / height**2

print(name + ", sua consulta foi realizada com sucesso! =)")
print("o seu IMC foi de " + str(imc) +", vocẽ receberá o exame completo")
print("pelo motivo", reason, "em", email)

divisaoReasonName = len(reason) / len(name)
divisaoNameReason = len(name) / len(reason)

if(divisaoReasonName.is_integer()):
    print("Parabéns! Você nunca mais pagará uma consulta na vida!")
else:
    print("Parabéns! Você acabou de ganhar um consulta DE GRATIS!")

if(float(('%.3f'%divisaoReasonName)) == 1.618 or float(('%.3f'%divisaoNameReason)) == 1.618):
    ("Você está devendo 400.000 mil reais para o consultório")

#calcular o IMC

#a saída do programa deve ser:
#[nome do animal], sua consulta foi realizada com sucesso! =)
#o seu IMC foi de [IMC calculado], vocẽ receberá o exame completo
#pelo motivo [motivação] em [email do animal]

#BONUS
#Se o numero de letras da string associada a var "reason"
#divido pelo numero de letras da string var "name"
#for um numero racional, o paciente ganhará um consulta DE GRATIS!

#Caso seja um divisão exata, não pagará nunca mais na vida
#por uma consulta

#e por fim caso esses números representem caetos de um
#triangulo retangulo perfeito está devendo 400.000 mil reais
#para o consultório
