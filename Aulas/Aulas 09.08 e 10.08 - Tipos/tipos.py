import sys

inteiro = 5
decimal = 3.4
booleano = True
string = "lorem ipsum"
lista = []

print(inteiro.__class__.__name__, sys.getsizeof(inteiro))
print(decimal.__class__.__name__, sys.getsizeof(decimal))
print(booleano.__class__.__name__, sys.getsizeof(booleano))
print(lista.__class__.__name__, sys.getsizeof(lista))
print(string.__class__.__name__, sys.getsizeof(string))

print("\nTransformar em int")
print(int(decimal))
print(int(booleano))
#print(int(string)) ValueErro
#print(int(lista)) TypeErro


print("\nTransformar em float")
print(float(inteiro))
print(float(booleano))
#print(float(string)) ValueErro
#print(float(lista)) TypeErro

print("\nTransformar em booleano")
print(bool(inteiro))
print(bool(decimal))
print(bool(string))
print(bool(lista))

print("\nTransformar em string")
print(str(inteiro))
print(str(decimal))
print(str(booleano))
print(str(lista))

print("\nTransformar em list")
#print(list(inteiro)) TypeErro
#print(list(decimal)) TypeErro
#print(list(booleano)) TypeErro
print(list(string))
