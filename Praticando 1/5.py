def ask_float(message, error_message = "not a float value"):
	try:
		v = float(input(message))
		return v
	except ValueError:
		print(error_message)
		return ask_float(message, error_message)

numero = 0

while(numero != 10):
    numero = ask_float("Insira um numero:")

print("Você não deveria ter digitado 10!")
