def pedir_numero():
    try:
        numero = float(input("Informe um número: "))
        if (numero > 5 and numero >= 8 and numero <= 12) or numero > 33 :
            print("Você ganhou!")
        else:
            print("Você não ganhou.")
    except ValueError as e:
        print("O valor informado precisa ser um número.")
        pedir_numero()


pedir_numero()
