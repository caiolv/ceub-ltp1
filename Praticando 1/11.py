week_days = ["segunda-feira",
             "terça-feira",
             "quarta-feira",
             "quinta-feira",
             "sexta-feira",
             "sabado",
             "domingo"]

def ask_cigars():
    cigars = input("Informe charutos: ")
    if(cigars == "open charutos"):
        return cigars
    else:
        try:
            integer = int(cigars)
            return integer
        except ValueError:
            print("Inválido.")
            return ask_cigars

def get_option():
    try:
        option = int(input("Escolha: "))
        return week_days[option-1]
    except (IndexError, ValueError):
        print("Opção inválida.")
        return get_option()

def ask_day():
    print("Selecione o dia da festa:")
    for i, day in enumerate(week_days):
        print("{} - {}".format(i+1, day.title()))
    return get_option()

day_chosen = ask_day()
number_of_cigars = ask_cigars()


if (day_chosen in week_days[-2:] and number_of_cigars == "open charutos") or (day_chosen not in week_days[-2:] and number_of_cigars >= 40 and number_of_cigars <= 60):
    print("Só sucesso.")
else:
    print("Festinha sem graça.")
