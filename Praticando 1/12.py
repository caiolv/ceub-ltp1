import math
def ask_int(message, error_message = "not a int value"):
	try:
		v = int(input(message))
		return v
	except ValueError:
		print(error_message)
		return ask_float(message, error_message)

def check_if_triangular(number):
    max = math.ceil(number ** (1. / 3))

    if(number == (max-1)*max*(max+1)):
        print("É um número triangular")
    else:
        print("Não é um número triangular")

number = ask_int("Informe o número: ")
check_if_triangular(number)
