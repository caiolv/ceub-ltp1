import os
import random

weapons = ["pedra", "papel", "tesoura"]
p1_score = 0
p2_score = 0

def show_options(player):
    os.system("clear")
    print(player, "escolha um:")
    print("1 - Pedra")
    print("2 - Papel")
    print("3 - Tesoura")
    return choose_weapon()

def choose_weapon():
    try:
        option = int(input("Opção: "))
        return weapons[option-1]
    except (ValueError, KeyError) as e:
        print("Opção inválida.")
        return choose_weapon()

def winner(player1, player2, comp):
    os.system("clear")
    player1_index = weapons.index(player1)
    player2_index = weapons.index(player2)
    global p1_score
    global p2_score
    player1_display = "Player" if comp else "Player 1"
    player2_display = "Computador" if comp else "Player 2"
    print(player1_display, "->", player1)
    print(player2_display, "->", player2, "\n")
    if(player1_index == player2_index):
        print("Empate!")
    elif (player1_index - player2_index) % 3 == 1:
        p1_score += 1
        print(player1_display, "ganhou!")
    else:
        p2_score += 1
        print(player2_display, "ganhou!")

    print("\n" + player1_display, ":", str(p1_score))
    print(player2_display, ":", str(p2_score))

def ask_if_player_2():
    comp = input("Modo um jogador? (S/N)")

    if(comp not in ["S", "s", "N", "n"]):
        return ask_continue()

    return comp == "S" or comp == "s"

def run_game(comp):
    if(comp):
        player1 = show_options("Jogador")
        player2 = weapons[random.randint(0, (len(weapons) - 1))]
    else:
        player1 = show_options("Player 1")
        player2 = show_options("Player 2")

    winner(player1, player2, comp)

def ask_continue():
    exit = input("Novo jogo? (S/N)")

    if(exit not in ["S", "s", "N", "n"]):
        return ask_continue()

    return exit == "S" or exit == "s"

def start():
    run = True
    comp = ask_if_player_2()

    while(run):
        run_game(comp)
        run = ask_continue()

start()
