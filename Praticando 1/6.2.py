def ask_string(message):
	v = input(message)
	return v

first = ask_string("Primeiro nome: ")
second = ask_string("Segundo nome: ")
full_name = first + " " + second
print("Seu nome é", full_name[::-1])
