def ask_int(message, error_message = "not a int value"):
	try:
		v = int(input(message))
		return v
	except ValueError:
		print(error_message)
		return ask_float(message, error_message)
def is_even(number):
    return(number % 2 == 0)

def count_ears(number):
    total_ears = 0

    if is_even(number):
        total_ears = (number / 2) * 5
    else:
        round_half = ((number + 1) / 2)
        total_ears = round_half * 2 + (number - round_half) * 3

    return total_ears

bunnies = ask_int("Informe o numero de coelinhos na fila: ")
print("Numero total de orelhas: ", "%.0f" % count_ears(bunnies))
