import random

def shuffle_lista(list_ordered):
    list_aux = []
    new_list = list_ordered[:]
    list_size = len(list_ordered) - 1

    for idx, val in enumerate(list_ordered):
        random_index = random.randint(0, list_size)

        if(random_index not in list_aux):
            new_list[idx] = list_ordered[random_index]
            list_aux.append(random_index)
        else:
            while(random_index in list_aux):
                random_index = random.randint(0, list_size)
            new_list[idx] = list_ordered[random_index]
            list_aux.append(random_index)

    return new_list

teste = ["A", "B", "C", "D", "E"]
print(teste)
print(shuffle_lista(teste))
