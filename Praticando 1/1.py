meses = ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho",
         "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"]

def pedir_numero():
    try:
        numero = int(input("Informe um número de 1 a 12: "))
        if(numero > len(meses) or numero < 1):
            print("O número informado precisa estar entre 1 e 12.")
            pedir_numero()

        print(meses[numero-1])
    except ValueError as e:
        print("O valor informado precisa ser um número inteiro.")
        pedir_numero()


pedir_numero()
