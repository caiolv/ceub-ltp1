def ask_int(message, error_message = "not a int value"):
	try:
		v = int(input(message))
		return v
	except ValueError:
		print(error_message)
		return ask_float(message, error_message)

def ask_q(message, q):
    p = ask_int(message)

    if(len(str(p)) > len(str(q))):
        print("O tamanho de p só pode ser menor ou igual a q.")
        return ask_q(message, q)

    return p

q = ask_int("Informe q: ")
p = ask_q("Informe p: ", q)

if(str(p) in str(q)):
    print("p é um subnúmero de q.")
else:
    print("p não é um subnúmero de q.")
