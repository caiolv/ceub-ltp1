def pedir_peso():
    try:
        peso = float(input("Informe o peso: "))
        if peso < 0 :
            print("O peso não pode ser negativo.")
            pedir_peso()
        else:
            print("O resultado da conversão de", peso, "pounds é", "{0:.2f}".format(peso*2.2), "kilogramas.")
    except ValueError as e:
        print("O valor informado precisa ser um número.")
        pedir_peso()


pedir_peso()
