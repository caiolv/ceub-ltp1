import os
messages = []

# def ask_message():
# 	message = input("Insira a mensagem: ")
#     if(len(message) > 140):
#         print("Muito grande.")
#         return ask_message()
def ask_message():
    message = input("Insira a mensagem: ")
    if(len(message) > 140):
        print("Muito grande.")
        return ask_message()
    else:
        messages.append(message)

def print_messages():
    for message in messages[::-1]:
        print(message)

    print("--------------")

def start():
    running = True

    while(running):
        os.system("clear")
        print_messages()
        ask_message()
start()
