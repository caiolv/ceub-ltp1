import os
weapons = ["pedra", "papel", "tesoura"]

def show_options(player):
    os.system("clear")
    print(player, "escolha um:")
    print("1 - Pedra")
    print("2 - Papel")
    print("3 - Tesoura")
    return choose_weapon()

def choose_weapon():
    try:
        option = int(input("Opção: "))
        return weapons[option-1]
    except (ValueError, KeyError) as e:
        print("Opção inválida.")
        return choose_weapon()

def winner(player1, player2):
    os.system("clear")
    player1_index = weapons.index(player1)
    player2_index = weapons.index(player2)

    print("Player 1:", player1)
    print("Player 2:", player2)
    print("\n")
    if(player1_index == player2_index):
        print("Empate!")
    elif (player1_index - player2_index) % 3 == 1:
        print("Player 1 ganhou!")
    else:
        print("Player 2 ganhou!")

def run_game():
    player1 = show_options("Player 1")
    player2 = show_options("Player 2")
    winner(player1, player2)

def ask_continue():
    exit = input("Novo jogo? (S/N)")

    if(exit not in ["S", "s", "N", "n"]):
        return ask_continue()

    return exit == "S" or exit == "s"

def start():
    run = True

    while(run):
        run_game()
        run = ask_continue()

start()
