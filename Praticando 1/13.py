def ask_int(message, error_message = "not a int value"):
	try:
		v = int(input(message))
		return v
	except ValueError:
		print(error_message)
		return ask_float(message, error_message)

def check_if_prime(number):
    for n in range(2, number):
        if(number % n == 0):
            return False
    return True

number = ask_int("Inserir numero: ")
print(check_if_prime(number))
