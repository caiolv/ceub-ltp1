from math import sqrt

def is_square(n):
    return sqrt(n)

def print_if_perfect_square(number):
    sum = int(str(number)[:2]) + int(str(number)[-2:])
    if is_square(number) == sum:
        print(number)

for i in range(1000, 10000):
    print_if_perfect_square(i)
