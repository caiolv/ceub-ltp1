import datetime

def ask_int(int_name, error = "not int"):
    try:
        integer = int(input(int_name))
        return integer
    except ValueError:
        print(erro)
        ask_int(int_name, error)

day = ask_int("Dia: ")
month = ask_int("Mês: ")
year = ask_int("Ano: ")

today = datetime.datetime.strptime(str(year) + '-' + str(month) + '-' + str(day),"%Y-%m-%d")
yesterday = today + datetime.timedelta(days=-1)
tomorrow = today + datetime.timedelta(days=1)
print(f"{yesterday:%d-%m-%Y}")
print(f"{tomorrow:%d-%m-%Y}")
